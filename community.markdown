---
titleOnlyInHead: 1
---

<h3>Source code</h3>

[<i class="fab fa-gitlab"></i> GitLab](https://gitlab.com/blacknet-ninja)


<h3>Community</h3>

[<i class="fas fa-comments"></i> Matrix room](https://riot.im/app/#/room/#blacknet:matrix.org) #blacknet:matrix.org

[<i class="fab fa-bitcoin"></i> BitcoinTalk](https://bitcointalk.org/index.php?topic=469640.0)

[<i class="fab fa-reddit"></i> Reddit](https://www.reddit.com/r/blacknet)

[<i class="fab fa-qq"></i> QQ group](https://www.qq.com/) 705602427

[<i class="fab fa-gitlab"></i> Activity in the GitLab group](https://gitlab.com/groups/blacknet-ninja/-/activity)


<h3>Exchanges</h3>

[<i class="fas fa-exchange-alt"></i> AEX.plus](https://www.aex.plus/page/trade.html#/?symbol=bln_cnc)

[<i class="fas fa-exchange-alt"></i> vinex.network](https://vinex.network/market/BTC_BLN) 


<h3>Explorers</h3>

[<i class="fas fa-cubes"></i> blnscan.io](https://www.blnscan.io/)


<h3>Staking Pools</h3>

[<i class="fas fa-cubes"></i> blnpool.io](https://www.blnpool.io/)
